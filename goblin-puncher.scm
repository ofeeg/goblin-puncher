(use-modules (goblin-puncher player)
	     (goblin-puncher world)
	     (goblin-puncher characters)
	     (goblin-puncher world-map)
	     (goblins))
;;(load "player.scm")
;;(load "mock_input.scm")
(load "goblin-puncher/locations.scm")
(define world-vat)
(define player-vat)
(define world)
(define player)
(define old-text "")
(define (init-goblin-puncher)
  (set! world-vat (spawn-vat))
  (call-with-vat world-vat
		 (lambda ()
		   (begin 
		     (set! world (list ^world (cons (car Entities) (map (lambda (x) (list-set! Entities (list-index Entities x) (apply spawn x))) (cdr Entities)))))
		     (set! world (apply spawn world))
		     (set! DUMMY (apply spawn DUMMY))
		     (set! locations (map (lambda (location) (apply spawn (append (list ^location) location))) locations))
		     ;;(set! world-map (map (lambda (location) (apply spawn (append (list ^location) location))) locations))
		     (set! world-map (map (lambda (location)  (cons (caddr ($ location 'get-info)) location)) locations))
		     )
		   )
		 )
  (set! player-vat (spawn-vat))
  
  )

(define (read-event-to-screen)
    (addstr stdscr (call-with-vat world-vat
		 (lambda ()
		   (begin
		     current-event
		     ))))
    )
;;(define (game-loop)


(use-modules (ncurses curses))


(init-goblin-puncher)

(call-with-vat player-vat (lambda () (begin (set! player (spawn ^player "Player" '(0 0)))
					($ player 'request-map-event)
					)))

(define stdscr (initscr))
(noecho!)
(define input #\0)
(read-event-to-screen)
(while (not (eq? input #\e));(input-loop 101 0) 101))
  (call-with-vat player-vat
		 (lambda ()
		   (begin
 		     ($ player 'request-map-event)
		     (define sending 'player-up)
		     (if (eq? input #\0) (noop)
			 (cond ((eq? input #\w) (set! sending 'player-up))
			   ((eq? input #\s) (set! sending 'player-down))
			   ((eq? input #\d) (set! sending 'player-right))
			   ((eq? input #\a) (set! sending 'player-left))
;;			   (else (error 'BadInput))
			   ))
   		     ($ player 'interact sending)
		     (noop))))
  (set! input (getch stdscr))
  (clear stdscr)
  (read-event-to-screen)
  ;;(addstr stdscr "Hello World!!!")
  (refresh stdscr)
  )
;; (addstr stdscr "Goodbye!!!")
;; (refresh stdscr)
;; (getch stdscr)
(endwin)
  
