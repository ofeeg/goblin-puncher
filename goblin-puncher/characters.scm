(define-module (goblin-puncher characters)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:export (
	    stats
	    ^effect
	    ^character
	    Player
	    Default
	    Goblin
	    DUMMY
	    )
  )
;;(use-modules (goblins) (goblins actor-lib methods))

(define stat-types (list 'HP 'ATK 'DEF 'INT 'MND 'SPD))

(define (stats list) ;;(ATK DEF INT MND SPD)
  (set! list (cons (* (car list) (cadr list)) list))
  (methods
   ((get-stats) list)
   ((HP)  (car list))
   ((ATK) (cadr list))
   ((DEF) (caddr list))
   ((INT) (cadddr list))
   ((MND) (car (list-tail list 4)))
   ((SPD) (car (list-tail  list 5)))
   )
  )

(define* (^effect bcom symbol amount removal-reqs decrement? #:optional (stat #f))
  (methods
   ((effect)
    symbol)
   ((amount)
    (if stat (list amount stat)
    (list amount)))
   ((increment num)
    (bcom (^effect  bcom symbol (+ amount num) removal-reqs decrement? stat)))
   ((manage-self)
    (if (null? removal-reqs) (noop)
	(bcom (^effect bcom 
		       (cond
			((and (member 'ZERO removal-reqs) (= amount 0)) 'Delete)
			((and (member 'NEGATIVE removal-reqs) (< amount 0)) 'Delete)
			(else symbol))
		       amount removal-reqs decrement? stat)))
    )
   )
  )

;;Tedious. Make an effect factory, and move effect-related stuff to effect.scm.
(define* (effect-manage effects effect amount #:optional (stat #f))
  (let ((existing-effect (filter (lambda (eff) (eq? ($ eff 'effect) effect)) effects)))
    (if (null? existing-effect)
	(list effect amount stat)
	($ (car existing-effect) 'increment amount))
    )
  )

(define DUMMY  (list ^effect 'Dummy #f #f #f #f))

(define* (^character bcom name desc stats skills #:optional (effects (list DUMMY)))
  (define (apply-stat-effect-beh)
    (map
     (lambda (effect)
       (if (not (null? (cdr ($ effect 'amount))))
	   (let ((apply-beh ($ effect 'amount)))
	     (list (cadr apply-beh) (stats (cadr apply-beh))
		   (let ((new-stat (- (stats (cadr apply-beh)) (car apply-beh))))
		     (cond  ((< new-stat 0) 0)
			    ((and (equal? 'HP (cadr apply-beh))
				  (> new-stat (stats 'HP)))
			     (stats 'HP)
			     )
			    (else new-stat))))
	     )
	   (noop)
	   )
       )
     (cdr effects)
     )
    )
  (methods
   ((manage-effects)
    (map (lambda (effect) ($ effect 'manage-self)) (cdr effects))
    (map (lambda (eff) (delq! eff effects)) (filter (lambda (eff) (equal? ($ eff 'effect) 'Delete)) (cdr effects)))
    (bcom (^character bcom name desc stats skills effects))
    )
   ((get-info)
    (list name desc (stats 'get-stats) skills (map (lambda (eff) (cons   ($ eff 'effect) ($ eff 'amount))) (cdr effects)))
    )
   ((get-stat stat)
    (stats stat)
    )
   ((get-current-stats)
    (let ((affected-stats (apply-stat-effect-beh)))
      (if (null? affected-stats)
	  (stats 'get-stats)
	  (let ((original-stats
		 (map-in-order (lambda (stat-type) (stats stat-type))  stat-types)))
	    (map (lambda (val) (list-set! original-stats (list-index stat-types (car val)) (caddr val))) affected-stats)
	    original-stats)
	  )
      )
    )
   ((affected effect amount)
    (let ((eff-obj (effect-manage (cdr effects) effect amount stat)))
      (if (list? eff-obj)
	  (cond
	   ((eq? effect 'Damage) (bcom (^character bcom name desc stats skills 
						   (append effects (list (spawn ^effect effect amount '(ZERO NEGATIVE) #f 'HP)))))
	    )
	   )
	  (bcom (^character bcom name desc stats skills effects)))
      )
    )
   )
  )


(define Player)
(define Default (list ^character "Default" "If you see this, then either you are hacking, or something went wrong."
		      (stats '(2 2 2 2 2)) '(Attack)))

(define Goblin (list ^character "Goblin" "A species of angry green humanoids. Some of whom demand superiority."
		      (stats '(3 2 1 1 2)) '(Attack)))


;;debug
(define w)
(define (debug-character)
  (set! w (spawn-vat))
   (call-with-vat w (lambda () (set! DUMMY (apply spawn DUMMY))))
  )
