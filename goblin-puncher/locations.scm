(define special-events ;;Location based mutations. (FLAG ( (COORDINATES (EVENTS NEW-FLAGS)) ... ) )
  '(
    INTRODUCED
    '('(0 0)
      '("The place you first started. You see the JobBoard in the distance. The Guild Recruiter looks in your direction." '()
	)
      )
    
    )
  )

;;List of locations. (EVENT '(FLAG1 FLAG2 ...) COORDINATES)
(define locations
  (list
   ( list
     "Welcome to the world of Gobillia! In this world, Goblins and Humans have coexisted for many millenia. During the last century,  a subsidiary of the Goblins have begun to revolt against the Humans. They have grown to an immense size and are harassing the Human Kingdom of Humbull. You, a lone warrior looking for some money and fame, have decided to answer Humbull's call for aid and join the war effort against the Goblin subsidiary: All For Goblins. You stand just before the job board outside of Humbull, a step away from the Guild recruiter who will give you your first job."
     (list 'INTRODUCED)
     '(0 0)
     )
   )
  )
