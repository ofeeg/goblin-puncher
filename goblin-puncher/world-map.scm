(define-module (goblin-puncher world-map)
  #:declarative? #f
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:export (
	    ^location
	    world-map
	    locations
	    find-matching-flags
	    flags
	    )
  )
;;(use-modules (goblins) (goblins actor-lib methods))

(load "locations.scm")

(define special-events) ;;Location based mutations. (FLAG ( (COORDINATES (EVENTS NEW-FLAGS)) ... ) )
(define flags (list 'player-up 'player-down 'player-right 'player-left))

(define (find-matching-flags flags events)
  (let ((matches (filter (lambda (res) (not (null? res)) )
			 (map
			  (lambda (event-flag)
			    (filter (lambda (flag) (equal? event-flag flag)) flags)) events))))
    (if (null? matches) matches (car matches))
    )
  )

(define (^location bcom event event-flags coordinates )
  (methods
   ((check-flags flags)
    (if (null? event-flags) (noop)
	(let ((matching-flags (find-matching-flags flags event-flags)))
	  (if (null? matching-flags) (noop)
	      (let ((new-event (car (cdadar (filter (lambda (locations-event) (eq? coordinates (cadr locations-event)))  (filter (lambda (event) (equal? (car matching-flags) (car event))) special-events))))))
		(bcom (^location bcom (car new-event) (if (null? (cadr new-event)) event-flags (cadr new-event)) coordinates))
		)
	      )
	  )
	)
    )
   ((get-info)
    (list event event-flags coordinates))
   )
  )

(define world-map) ;;List of locations


(define w)
(define location)
(define (debug-map)
  (set! w (spawn-vat))
  (set! special-events (list (list 'asdf '((0 0) ("asdf" ())))))
  (set! flags (append flags '(asdf)))
  (call-with-vat w (lambda ()
		     (set! location (spawn ^location "This is a place" '(asdf) '(0 0)))
		     (set! world-map (list (list (cadr ($ location 'get-info)) location ))) )
		 )

  )
