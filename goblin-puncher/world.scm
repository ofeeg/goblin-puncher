(define-module (goblin-puncher world)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblin-puncher characters)
  #:export (
	    Entities
	    ^world
	    Fighter-Stats
	    player-creator
	    )
  )
	    
	    
	    
;;(use-modules (goblins) (goblins actor-lib methods) (goblin-puncher characters))
;;(load "./characters.scm")

(define Entities (list Player Default Goblin)) 
(define (damage-calculation attacking-stat defending-stat ability-potency)
  (round (/ (+ ability-potency attacking-stat) defending-stat)))

(define (defense-check stat-type)
  (if (eq? 'ATK) 'DEF 'MND))

(define Abilities (list '(Attack ATK Damage 1)))

(define (^world bcom inhabitants)
  (define (find-inhabitant name)
    (filter (lambda (entity) (equal? name (cdr entity)))
	    (map (lambda (object) 
		   (cons (list-index inhabitants object) (car ($ object 'get-info)))
		   )
		 (cdr inhabitants))))
  (methods
   ((get-inhabitants)
    inhabitants)
   ((init-player player-params)
    (let ((player
	   (catch 'InvalidData
	     (lambda ()
	       (player-creator (car player-params) (cadr player-params) (caddr player-params))
	       )
	     (lambda (key . args)
	       (player-creator (car player-params) 'Fighter '())
	       )
	     )
	   ))
      (bcom (^world bcom (cons player inhabitants))))
    )
   ((encounter name)
    (catch 'InvalidData
      (lambda ()
	(let ((entity (filter (lambda (entity) (equal? name (cdr entity)))
			      (map (lambda (object) 
				     (cons (list-index inhabitants object) (car ($ object 'get-info)))
				     )
				   (cdr inhabitants)))
		      ))
	  (if (null? entity) (throw 'InvalidData "Name does not exist.")
	      (list-ref inhabitants (caar entity))
	      )
	  )
	)
      (lambda (key . args)
	(cadr inhabitants))
      )
    )
   ((affect target affector ability)
    (catch 'AffectorLacksAbility
      (lambda ()
	(if (null? (filter (lambda (affectors-ability) (eq? ability affectors-ability)) (cadddr ($ (list-ref inhabitants (caar (find-inhabitant affector))) 'get-info)))) (throw 'AffectorLacksAbility "Entity does not have ability.")
	    (let ((ability
		   (filter (lambda (ability-name) (eq? (car ability-name) ability)) Abilities)) (affector-stat ($ (list-ref inhabitants (caar  (find-inhabitant affector))) 'get-stat (cadar (filter (lambda (ability-name) (eq? (car ability-name) ability)) Abilities))))
		  )
	      (catch 'AbilityNotFound
		(lambda ()
		  (if (null? ability) (throw 'AbilityNotFound "Ability doesn't exist.")
		      (cond ((eq? 'Damage (caddar ability))
			     ($ (list-ref inhabitants (caar (find-inhabitant target))) 'affected (caddar ability)
				(damage-calculation affector-stat
						    ($ (list-ref inhabitants (caar (find-inhabitant target))) 'get-stat (defense-check (cadar ability)))
						    (cadddr (car ability)))))
			    (else (throw 'AbilityNotFound "Ability Type not accounted for."))
			    )
		      )
		  )
		(lambda (key . args)
		  (error args)
		  )
		)
	      )
	    )
	)
      (lambda (key . args)
	  (error args)
	  )
      )
    )
   )
  )
  
  
(define Fighter-Stats (stats (list 4 2 1 2 3)))

(define (player-creator name class stat-boosts)
  (if (> (length stat-boosts) 3) (throw 'InvalidData "Invalid amount of boosts.")
      (cond
       ((eq? class 'Fighter) (spawn ^character name (stats (boost (cdr (Fighter-Stats 'get-stats)) stat-boosts 0)) '(Attack)))
       (else (throw 'InvalidData "Class does not exist."))
       )
      )
  )

(define (boost list boosts count)
  (if (eq? count (length list)) '()
      (if (null? (filter (lambda (x) (eq? count x)) boosts))
	  (cons (list-ref list count) (boost list  boosts (1+ count)))
	  (cons (+ (list-ref list count)
		   (length (filter (lambda (x) (eq? count x)) boosts))) (boost list boosts (1+ count)))
	  )
      )
  )

;; Add entities by adding them to the bottom of characters.scm, and then add them to the list of entities.
;;(define world (list ^world (cons (car Entities) (map (lambda (x) (list-set! Entities (list-index Entities x) (apply spawn x))) (cdr Entities)))))



;;debug
(define w)
(define world)
(define (debug-world)
  (set! w (spawn-vat))
  (call-with-vat w (lambda () (set! world (list ^world (cons (car Entities) (map (lambda (x) (list-set! Entities (list-index Entities x) (apply spawn x))) (cdr Entities)))))
		   (set! world (apply spawn world)) (set! DUMMY (apply spawn DUMMY)))
	     )
)
