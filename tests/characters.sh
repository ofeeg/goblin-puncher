#!/usr/bin/env bats


@test "Character initialization" {
    run  exec guile  characters.scm -lc "(debug-character) (call-with-vat w (lambda () (apply spawn Default))) "
    [ "${status}" -eq 0 ]
}

@test "Character getters" {
    run exec guile  characters.scm -lc "(debug-character) (call-with-vat w (lambda () ($ (apply spawn Default) 'get-info) ($ (apply spawn Default) 'get-current-stats) ($ (apply spawn Default) 'get-stat 'ATK)))"
    [ "${status}" -eq 0 ]
}

@test "Character apply effect" {
    run exec guile  characters.scm -lc "(debug-character) (call-with-vat w (lambda () ($ (apply spawn Default) 'affected 'Damage 0)))"
    [ "${status}" -eq 0 ]
}

@test "Character manage effects" {
    run exec guile  characters.scm -lc "(debug-character) (call-with-vat w (lambda () (let ((test (apply spawn Default))) ($ test 'affected 'Damage 0) ($ test 'manage-effects) (if (null? (car (list-tail ($ test 'get-info) 4))) #t (error 'manage-effect-error)))))"
}


# @test "Dummy" {
#     run exec guile -l characters.scm -c "(display 'asdf)"
#     [ "${status}" -eq 0 ]    
# }
