#!/usr/bin/env bats

@test "Display location" {
    run  exec guile world-map.scm -lc "(debug-map) (call-with-vat w (lambda () ($ location 'get-info))) "
    [ "${status}" -eq 0 ]
}

@test "Display all locations" {
    run  exec guile  world-map.scm -lc "(debug-map) (call-with-vat w (lambda () (if (equal? (car world-map) (list  (cadr ($ location 'get-info)) location)) #t (error 'MapError))))"
    [ "${status}" -eq 0 ]
}

@test "Location consume flag and mutate" {
    run  exec guile  world-map.scm -lc "(debug-map) (call-with-vat w (lambda () ($ location 'check-flags  flags) (if (equal? (car ($ location 'get-info)) \"asdf\") #t (error 'LocationError))))"
    [ "${status}" -eq 0 ]
}
