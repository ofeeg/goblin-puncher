#!/usr/bin/env bats

@test "Create player interface" {
    run  exec guile  player.scm -lc "(debug-player) (call-with-vat w (lambda () (if (equal? \"Player\" (car ($ player 'info))) #t (error 'PlayerCreationError))))"
    [ "${status}" -eq 0 ]
}

@test "Player request information from World to display." {
    run  exec guile  player.scm -lc "(debug-player) (call-with-vat w (lambda () ($ player 'request-map-event))) (sleep 2) (if (equal? \"This is a place\" current-event) #t (error 'PlayerRequestError))"
    [ "${status}" -eq 0 ]
}

@test "Player send input flag" {
    run  exec guile  player.scm -lc "(debug-player) (call-with-vat w (lambda () ($ player 'interact (list 'player-up)))) (sleep 1) (if (equal? tooltip \"Can't do that.\") #t (error 'PlayerInteractError))"
    [ "${status}" -eq 0 ]
}
