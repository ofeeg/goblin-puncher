#!/usr/bin/env bats

@test "World initialization" {
    run  exec guile world.scm -lc "(debug-world) (call-with-vat w (lambda () ($ world 'get-inhabitants))) "
    [ "${status}" -eq 0 ]
}

@test "World affect Default" {
    run  exec guile world.scm -lc "(debug-world) (call-with-vat w (lambda () ($ world 'affect \"Default\" \"Default\" 'Attack) (if (car (list-tail ($ (cadr Entities) 'get-info) 4)) #t (error 'affect-error))))"
    [ "${status}" -eq 0 ]
}

@test "World encounter Default" {
    run  exec guile  world.scm -lc "(debug-world) (call-with-vat w (lambda () (let ((encounter ($ world 'encounter \"Default\" ))) (if (eq? encounter (cadr Entities)) #t (error 'encounter-error)))))"
    [ "${status}" -eq 0 ]
}
